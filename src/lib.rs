use pyo3::prelude::*;
use regex::Regex;

/// Return all non-overlapping matches of pattern in string, as a list of strings.
///
/// https://docs.python.org/3/library/re.html#re.findall
#[pyfunction]
fn findall<'a>(pattern: &str, string: &'a str) -> PyResult<Vec<&'a str>> {
    let re = Regex::new(pattern).expect("Failed to construct regex.");
    let matches = re.find_iter(string).map(|m| m.as_str()).collect();
    Ok(matches)
}

#[pyclass]
struct Match {
    #[pyo3(get)]
    group: String,
    #[pyo3(get)]
    start: usize,
    #[pyo3(get)]
    end: usize,
}

/// Scan through string looking for the first match of the pattern,
/// and return a corresponding Match.
/// Return None if no position in the string matches the pattern.
///
/// https://docs.python.org/3/library/re.html#re.search
#[pyfunction]
fn search<'a>(pattern: &str, string: &'a str) -> Option<Match> {
    let re = Regex::new(pattern).expect("Failed to construct regex.");
    if let Some(m) = re.find(string) {
        Some(Match {
            group: m.as_str().into(),
            start: m.start(),
            end: m.end(),
        })
    } else {
        None
    }
}

/// Python rusty_regex main module implemented in Rust.
#[pymodule]
fn rusty_regex(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(findall, m)?)?;
    m.add_function(wrap_pyfunction!(search, m)?)?;
    Ok(())
}
