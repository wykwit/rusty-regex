import os
import random
from http.server import SimpleHTTPRequestHandler, HTTPServer

server = ("127.0.0.1", 8000)
host = f"http://{server[0]}:{server[1]}/"
files = 1200


def random_url():
    i = str(random.randint(1, files))
    return host + i + " "


def random_file():
    links = random.randint(0, files // 10)
    data = b""
    for i in range(links):
        b = random.randint(0, 1024)
        d = random.randbytes(b)
        data += d + random_url().encode()
    return data


def clean():
    print("Cleaning up")
    for i in range(1, files + 1):
        try:
            os.remove(str(i))
        except:
            pass
    try:
        os.remove("index.html")
    except:
        pass


def main():
    clean()
    input("Continue?")

    print("Generating random files")
    for i in range(1, files + 1):
        with open(str(i), "wb") as f:
            f.write(random_file())

    print("Generating random index.html")
    with open("index.html", "w") as f:
        f.write("\n".join([random_url() for _ in range(files // 2)]))

    print("Running HTTP server")
    print(host)
    httpd = HTTPServer(server, SimpleHTTPRequestHandler)
    httpd.serve_forever()


if __name__ == "__main__":
    main()
