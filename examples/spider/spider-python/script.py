import json
import pprint
import queue
import re
import threading
import time

import requests

with open("config.json", "r") as f:
    config = json.load(f)

depth_limit = config.get("depth_limit", 2)
monitor_update = config.get("monitor_update", 2)
http_timeout = config.get("http_timeout", 3)
workers = config.get("workers", 32)
query = config.get(
    "query",
    "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",
)
entry = config.get("entry")
out_f = config.get("out_f")

q = queue.Queue()
r = requests.session()

lock = threading.Lock()
seen = set()
blacklist = set(config.get("blacklist", []))


def ok(url):
    lock.acquire()
    if url in seen or any([x in url for x in blacklist]):
        lock.release()
        return False
    else:
        seen.add(url)
        lock.release()
        return True


def worker():
    while True:
        url, depth = q.get()

        if depth == depth_limit or not ok(url):
            q.task_done()
            continue

        try:
            with r.get(url, timeout=http_timeout) as resp:
                data = resp.text
        except:
            q.task_done()
            continue

        out = re.findall(query, data)
        for x in out:
            q.put((x, depth + 1))
        q.task_done()


def monitor():
    while True:
        status = {
            "queue_size": q.qsize(),
            "unfinished_tasks": q.unfinished_tasks,
            "urls_seen": len(seen),
        }
        pprint.pprint(status)
        time.sleep(monitor_update)


def main():
    q.put((entry, 0))
    for _ in range(workers):
        threading.Thread(target=worker, daemon=True).start()
    threading.Thread(target=monitor, daemon=True).start()
    q.join()
    if not out_f:
        return
    with open(out_f, "w") as f:
        f.write("\n".join(sorted(seen)))


if __name__ == "__main__":
    main()
