import random

from common import *

query = "test"
data = "".join(random.choices("qwertyuiopasdfghjklzxcvbnm", k=2400)) + query


@timed
def py(q, d):
    return re.search(q, d).group(0)


@timed
def rs(q, d):
    return rr.search(q, d).group


# run_test("search", py, rs, query, data)
repeat(1000, run_test, ("search", py, rs, query, data))
