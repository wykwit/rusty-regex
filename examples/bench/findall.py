import requests

from common import *

query = "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
with requests.get("https://en.wikipedia.org/wiki/Regular_expression") as r:
    data = r.text


@timed
def py(q, d):
    return re.findall(q, d)


@timed
def rs(q, d):
    return rr.findall(q, d)


# run_test("findall", py, rs, query, data)
repeat(1000, run_test, ("findall", py, rs, query, data))
