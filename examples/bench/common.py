import re
import rusty_regex as rr
import time


def timed(func):
    def _f(*args):
        start = time.time_ns()
        r = func(*args)
        end = time.time_ns()
        return (end - start, r)

    return _f


def run_test(name, py, rs, q, d, silent=False):
    py_t, py_r = py(q, d)
    rs_t, rs_r = rs(q, d)

    if not silent:
        print(f"Test: \t{name}")
        print(f"Python:\t{py_t}")
        print(f"Rust: \t{rs_t}")
    assert py_r == rs_r, "results are not the same"
    return (py_t, rs_t)


def repeat(n, f, args):
    p, r = 0, 0
    for i in range(n):
        _p, _r = f(*args, silent=True)
        p, r = p + _p, r + _r
    print(f"Results after {n} runs...")
    print(f"Python:\t{p}")
    print(f"Rust: \t{r}")
    return (p, r)
