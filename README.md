# rusty-regex

Comparing the speed of Python and Rust with PyO3 regex bindings.

## build

To compile the Rust library and automatically install the Python wheel:
```
$ maturin develop -r
```

## results

```
$ python examples/bench/findall.py
Results after 1000 runs...
Python:	1293390512
Rust: 	391743657
```

It's impressive how surprisingly fast `re.search` is in Python.
```
$ python examples/bench/search.py
Results after 1000 runs...
Python:	4502153
Rust: 	7958912
```

## spider

There is a simple web spider example included to showcase a practical usage of regex.

Run a server first to have something to spider.
```
$ cd examples/spider/server
$ python run.py
```

This will generate a few MBs of files randomly containing URL strings pointing at each other.
It will also create `index.html` file to easily get started scraping this.
Then a web server will start and display your starting URL.
When you run this program again it will clean up all the generated files first.
You can either continue and generate new testing set or quit at that point.

The spider takes a simple `config.json` from the working directory.
A default example is included, so have a look if you want to adjust any values.

The main implementation is in `examples/spider/spider-python/script.py`.
It's using a worker pool of threads to request new pages and then passes the content to a regex library.
In `spider-hybrid` we've replaced the default regex library with our own, not changing anything else.
Run it yourself.

```
$ cd examples/spider
$ python spider-hybrid/script.py
```

